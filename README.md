- IDE Used: Visual Studio Code

- Extensions: 
    Beautify
    Docker
    ESLint
    GitLab Workflow
    JavaScript (ES6) code snippets
    JavaScrip and TypeScript Nightly
    k6 for Visual Studio Code

- To run a build and test:
    CI/CD -> Pipeline -> Run Pipeline (You can run as many Pipelines as you like to run multiple tests)

- To review the results of the runs:
    CI/CD -> Jobs
    
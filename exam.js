//This K6 script will make different method calls to 'https://jsonplaceholder.typicode.com/
//Numbers will be randomly generated because in the real world randomly passing values etc., will prevent and/or minimize caching
//Created by: Byron Dorman

import http from 'k6/http';
import { group, check, sleep } from 'k6';

//Setting duration and Vu's.  ramp up 10s user within 20s.  Run the 10 users for 10s and then ramp down to 0.
export let options = {
  stages: [
    { duration: '20s', target: 10},
    { duration: '30s', target: 10},
    { duration: '1s', target: 0}
  ],
};

//add constants
const JPH_URL = 'https://jsonplaceholder.typicode.com'; 
const HEADERS = {'Content-type': 'application/json; charset=UTF-8'};
const POSTS = 'posts';
const COMMENTS = 'comments';
const MAX_SLEEP = 10;

//This function will generate a random number between 1 and max
export function randno(max) {

  let r_num = Math.floor(Math.random() * (max - 1 + 1) + 1);
  return r_num;
}

//This function gets the count of entries
export function ctprop(obj) {

  let count = obj.json().length;  
 
  return count;
}

export default function () {

//get the number of entries from each of the resources.  Real world these number would change over time.

//Posts Resource
let get_post_res = http.get(`${JPH_URL}/${POSTS}`);
check(get_post_res, { 'status were 200 for posts': (get_post_res) => get_post_res.status == 200 }); //making sure that Http status code 200 is returned 
let get_posts_ct = ctprop(get_post_res); //get count
//console.log("***Total number of post entries is " + get_posts_ct + "***");

//Comments Resource
let get_comment_res = http.get(`${JPH_URL}/${COMMENTS}`);
check(get_comment_res, { 'status were 200 for comments': (get_comment_res) => get_comment_res.status == 200 }); //making sure that Http status code 200 is returned
let get_comments_ct = ctprop(get_comment_res);
//console.log("***Total number of comment entries is " + get_comments_ct + "***");


//This group will execute the GET method
  group("GETS Method Group", function() {
    let rand_num = randno(get_posts_ct);
    //Uncomment the below to confirm that the number as being randomized
    //console.log(`***Retrieved post: ${JPH_URL}/${POSTS}/` + rand_num + `***`);

    let get_res = http.get(`${JPH_URL}/${POSTS}/` + rand_num); //Makes the api call
    check(get_res, { 'status were 200': (get_res) => get_res.status == 200 }); //making sure that Http status code 200 is returned

    let thinktime = randno(MAX_SLEEP); 
    sleep(thinktime);
  });

  //This group will execute the POST method and POST to the post resource and it will DELETE the created post
 group("POSTS & DELETE Methods Group", function() {

    const title = "This is my simple title!!!";
    const note = `this is my body text`;
    const body = {title: title, body: note, userid:`${__VU}`};

    let post_res = http.post(`${JPH_URL}/${POSTS}/`,JSON.stringify(body),{headers: HEADERS}); //Makes the api call
    check(post_res, { 'id number(s) was created': (post_res) => post_res.json().hasOwnProperty('id')});//making sure that an ID was created.

    const id = post_res.json()['id'];

    let add_note = post_res.json(['body']);

    //Uncomment the below to confirm that an id was created
    //console.log('***The id number created was:' + id + `***`);

    let thinktime = randno(MAX_SLEEP); 
    sleep(thinktime);

    let del_res = http.del(`${JPH_URL}/${POSTS}/`+ id); //Makes the api call to del the post
    check(del_res, {'200 received after deletes': (del_res) => del_res.status == 200});
      
  });


  //This group will execute the PUT method and will change the body in the comments resource
   group("PUTS Method Group", function() {

    let rand_num = randno(get_comments_ct);
    const note = `I have updated the body and my name is VU ${__VU}`;
    
    let get_put = http.get(`${JPH_URL}/${COMMENTS}/` + rand_num ); //getting a random comment
    check(get_put,{ 'status were 200': (get_put) => get_put.status == 200 }); //making sure that Http status code 200 is returned 
  
    const old_body = get_put.json('body'); //Get the old body text  
    const newbody = { body: `${note}`};
    let put_res = http.put(`${JPH_URL}/${COMMENTS}/` + rand_num,JSON.stringify(newbody),{headers: HEADERS}); //Makes the api call to change the body text
    check(put_res, { 'body(s) is/are present': (put_res) => put_res.json().hasOwnProperty('body')});//making sure that an ID is present.
    let newbody2 = put_res.json('body'); //Getting the updated body text

    //Uncomment the below to give that the PUT method worked
    //console.log('***The old body text was ' + old_body + ' ||| The new body text is ' + newbody2 + `***`); 

    let thinktime = randno(MAX_SLEEP); 
    sleep(thinktime);
  });

}
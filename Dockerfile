FROM loadimpact/k6
USER root
RUN  mkdir -p /exam
COPY exam.js /exam
WORKDIR /exam
CMD ["run", "exam.js" ]